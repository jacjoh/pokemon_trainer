# Pokemon_Trainer

This is an angular project

In this application you can as a trainer go through the pokedex, and mark pokemons as caught by you

When starting the application you need to type your trainer name to get access to the other pages on the application. After submitting the trainer name you are redirected to the pokemon list. Here are all the pokemons displayed in a table. You can click on a row to display a pokemons details. The pokemon details page contains all the information about a pokemon, and here you can catch the pokemon by pressing the catch button. The caught pokemons can be found at My pokemons in the header, or by navigating to '/trainer-page'. This page has the same functionality as the pokemon list. The trainers pokemons are stored locally, and one can catch more than one of each pokemon. 

