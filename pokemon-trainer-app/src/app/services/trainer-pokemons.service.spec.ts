import { TestBed } from '@angular/core/testing';

import { TrainerPokemonsService } from './trainer-pokemons.service';

describe('TrainerServiceService', () => {
  let service: TrainerPokemonsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(TrainerPokemonsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
