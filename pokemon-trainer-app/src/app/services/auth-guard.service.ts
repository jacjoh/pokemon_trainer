import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {
  routeURL: string;

  constructor(private router: Router) {this.routeURL = this.router.url;}

  // the Router call canActivate() method
  canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Promise<boolean> {
    // Here we can check if the user have entered a trainer name
    return new Promise((resolve, reject) => {
      if (localStorage.getItem('trainerName') != null){
        this.routeURL = this.router.url;
        return resolve(true);
      }
      //If the user has not entered the name he is redirected to the loading screen
      else {
        this.routeURL = '/loading-page';
        this.router.navigate(['/loading-page']);
        return resolve(false);
      }
    });
  }
}