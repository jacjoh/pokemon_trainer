import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { promise } from 'protractor';
import { PokemonListData } from '../components/pages/interfaces/pokemonListData';
import { PokemonInfo } from '../components/pages/interfaces/pokemonInfo';
import { Pokemon } from '../components/pages/interfaces/pokemon';

@Injectable({
  providedIn: 'root'
})

//A service for communicating with the pokemon API
export class PokemonAPIService {
  //Base url
  url: string = 'https://pokeapi.co/api/v2/';

  constructor(private http: HttpClient) {  }

  //Gets the initial list of pokemons
  public getPokemons(): Promise<any> {
    return this.http.get(`${this.url}pokemon?limit=50&offset=0`)
    .toPromise().then(data => {return this.getPokemonInfo(data, '')});
  } 

  //Gets all pokemons
  public getAllPokemonsWithName(searchWord): Promise<any> {
    return this.http.get(`${this.url}pokemon?limit=1050&offset=0`)
    .toPromise().then(data => {return this.getPokemonInfo(data, searchWord)})
  }

  //Go through each pokemon in the list and collect data
  private async getPokemonInfo(data, searchWord): Promise<any> {
    var pokemons: PokemonInfo = {
      next_url: '',
      previous_url: '',
      pokemonList: []
    };
    
    pokemons.next_url = data.next != null ? data.next : '';
    pokemons.previous_url = data.previous != null ? data.previous : '';

    //Iterate through the pokemons and get their data
    data.results.forEach(pokemon => {
      //See if the pokemon name includes the search word
      if(pokemon.name.includes(searchWord))
      {this.getDataFromPokemon(pokemons.pokemonList, pokemon.url);}
    },
    (error) => console.log(error),
    () => console.log("complete")
    );
    
    return pokemons;
  }
  
  //Collects data from each pokemon and adds it as an object to the list of pokemons
  private async getDataFromPokemon (list, pokemonUrl): Promise<any> {
    await this.getPokemon(pokemonUrl).then(async pokemonInfo => {
      this.insertSorted(list, {
        id: pokemonInfo.id,
        name: pokemonInfo.name,
        sprite: pokemonInfo.sprites.front_default,
        types: pokemonInfo.types
      }, this.compareById)}
    );
  }

  //Insert the pokemon at the right index of the pokemon list
  private insertSorted (array, element, comparator): void {
    for (var i = 0; i < array.length && comparator(array[i], element) < 0; i++) {}
    array.splice(i, 0, element)
  }
  
  //Compares the id of two pokemons
  private compareById (a, b): number { 
    return a.id - b.id 
  }
  
  //gets the next list of pokemons based on the url passed
  public getNextPokemons(url): Promise<any> {
    return this.http.get(url).toPromise().then(data => {return this.getPokemonInfo(data, '')});
  }

  //get a pokemon based on its url
  public getPokemon(pokemonUrl): Promise<any> {
    return this.http.get(pokemonUrl).toPromise();
  }

  //get a pokemon based on its id
  public getPokemonById(id): Promise<any> {
    return this.http.get(`${this.url}pokemon/${id}`).toPromise();
  }

  //Go through a list of pokemon ids and add from each pokemon to an array  
  public async getListOfPokemonsById(pokemonList:Array<string>): Promise<any> {
    var trainerPokemons = [];
    
    pokemonList.forEach(pokemonId => {
      let pokemonUrl: string = `${this.url}pokemon/${parseInt(pokemonId)}`
      this.getDataFromPokemon(trainerPokemons, pokemonUrl);
    });
    
    return trainerPokemons;
  }
}
