import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TrainerPokemonsService {

  constructor() { }

  public getPokemons(): Array<string> {
    let pokemons = localStorage.getItem('savedPokemons');
    return pokemons === null ? [] : pokemons.split(','); 
  } 

  public savePokemons(pokemons): void {
    localStorage.setItem('savedPokemons', pokemons.toString());
  }
}
