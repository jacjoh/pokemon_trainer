import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from "@angular/common/http";
import { CommonModule } from "@angular/common";
import { FormsModule }   from '@angular/forms';
 import { ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PokemonListComponent } from './components/pages/pokemon-list/pokemon-list.component';
import { PokemonInfoComponent } from './components/pages/pokemon-info/pokemon-info.component';
import { TrainerPageComponent } from './components/pages/trainer-page/trainer-page.component';
import { LoadingPageComponent } from './components/pages/loading-page/loading-page.component';
import { HeaderComponent } from './components/header/header.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    PokemonListComponent,
    PokemonInfoComponent,
    TrainerPageComponent,
    LoadingPageComponent,
    HeaderComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    CommonModule,
    NgbModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
