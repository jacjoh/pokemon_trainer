import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoadingPageComponent } from './components/pages/loading-page/loading-page.component';
import { PokemonInfoComponent } from "./components/pages/pokemon-info/pokemon-info.component";
import { PokemonListComponent } from "./components/pages/pokemon-list/pokemon-list.component";
import { TrainerPageComponent } from './components/pages/trainer-page/trainer-page.component';
import { AuthGuardService } from './services/auth-guard.service';

//If there are no trainer name, the user can only access the loading-page
const routes: Routes = [
  //The pokemon info page takes an id as a parameter
  { path: 'pokemon-info/:id', component: PokemonInfoComponent, canActivate: [AuthGuardService] },
  { path: 'pokemon-list', component: PokemonListComponent, canActivate: [AuthGuardService] },
  { path: 'trainer-page', component: TrainerPageComponent, canActivate: [AuthGuardService] },
  { path: 'loading-page', component: LoadingPageComponent},
  { path: '',   redirectTo: '/loading-page', pathMatch: 'full' }, // redirect to `first-component`
  { path: '**', component: PokemonListComponent },  // Wildcard route for a 404 page
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
