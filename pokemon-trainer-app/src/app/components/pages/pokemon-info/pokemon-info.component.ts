import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';
import { TrainerPokemonsService } from 'src/app/services/trainer-pokemons.service';
import { Pokemon } from '../interfaces/pokemon';
import {Location} from '@angular/common';

@Component({
  selector: 'app-pokemon-info',
  templateUrl: './pokemon-info.component.html',
  styleUrls: ['./pokemon-info.component.scss']
})
export class PokemonInfoComponent implements OnInit {
  //pokemon interface
  pokemon: Pokemon;

  constructor(
    private route: ActivatedRoute,
    private pokemonService: PokemonAPIService,
    private trainerService: TrainerPokemonsService,
    private _location: Location
  ) { }

  ngOnInit(): void {
    //Get the id of the pokemon from the url and collect it's data
    let id:number = parseInt(this.route.snapshot.paramMap.get('id'));
    this.getPokemonInfo(id);
  }

  //Get the data based on the pokemon's id
  async getPokemonInfo(id): Promise<void> {
    this.pokemon = await this.pokemonService.getPokemonById(id).then(r=>r);
  }

  //Adds the id of the pokemon to the list of pokemons catched
  catchPokemon():void {
    let pokemons = this.trainerService.getPokemons();
    pokemons.push(this.pokemon.id.toString());
    this.trainerService.savePokemons(pokemons);
  }

  //Return to the previous page
  GoBackOnePage(): void {
    this._location.back();
  }

  //Sets the display element of moves and the name of the button
  showMoves() {
    let elMoves = document.getElementById('moves');
    let elMovesButton = document.getElementById('moves-btn');

    if(elMoves.style.display == "inline") 
    {
      elMovesButton.innerText = "Show";
      elMoves.style.display = "none";
    }
    else
    {
      elMovesButton.innerText = "Hide";
      elMoves.style.display = "inline";
    }
  }

  //Sets the table's background color
  setTableBackgroundColor(type) {
    switch (type) {
      case 'grass': 
        return '#35e935';
      case 'fire': 
        return '#f15a5a';
      case 'poison': 
        return '#cb35e9';
      case 'flying': 
        return '#7c47dd';
      case 'bug': 
        return '#57701b';
      case 'normal': 
        return '#63621f';
      case 'dark': 
        return '#36362e';
      case 'electric': 
        return '#e9d735';
      case 'psychic': 
        return '#e935bc';
      case 'ground': 
        return '#8d8d58';
      case 'ice': 
        return '#35dde9';
      case 'steel': 
        return '#919191';
      case 'fairy': 
        return '#d87fd1';
      case 'water': 
        return '#3550e9';
      case 'fighting': 
        return '#e93535';
      case 'rock': 
        return '#4f522a';
      case 'ghost': 
        return '#4e0c7a';
      case 'dragon': 
        return '#404ea1';
      default:
        return '#ffffff'
    }
  }

  //Sets the row's background color, the color differs from the table's color
  setRowBackgroundColor(type) {
    switch (type) {
      case 'grass': 
        return '#77ff77';
      case 'fire': 
        return '#ff8a8a';
      case 'poison': 
        return '#e66aff';
      case 'flying': 
        return '#a473ff';
      case 'bug': 
        return '#8da84d';
      case 'normal': 
        return '#b1af5f';
      case 'dark': 
        return '#646462';
      case 'electric': 
        return '#fff385';
      case 'psychic': 
        return '#ff7ade';
      case 'ground': 
        return '#c4c490';
      case 'ice': 
        return '#78f1fa';
      case 'steel': 
        return '#b4b4b4';
      case 'fairy': 
        return '#f7a9f0';
      case 'water': 
        return '#6b81ff';
      case 'fighting': 
        return '#ff7777';
      case 'rock': 
        return '#737749';
      case 'ghost': 
        return '#6f299e';
      case 'dragon': 
        return '#6c7aca';
      default:
        return '#ffffff'
    }
  }

  //Shortens the base stat's name, to save some space when displaying
  changeDisplayedBaseStatName(name) {
    switch (name) {
      case 'hp':
        return 'HP';
      case 'attack':
        return 'Atk'; 
      case 'defense':
          return 'Def';
      case 'special-attack':
        return 'Sp.Atk'
      case 'special-defense':
        return 'Sp.Def'
      case 'speed':
        return 'Speed'
      default:
        return '';
    }
  }
}
