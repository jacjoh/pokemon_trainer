import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { PokemonAPIService } from "../../../services/pokemon-api.service";
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { PokemonListData } from '../interfaces/pokemonListData';
import { PokemonInfo } from '../interfaces/pokemonInfo';

@Component({
  selector: 'app-pokemon-list',
  templateUrl: './pokemon-list.component.html',
  styleUrls: ['./pokemon-list.component.scss']
})
export class PokemonListComponent implements OnInit {
  pokemonList: Array<PokemonListData> = []; 
  url: string = 'https://pokeapi.co/api/v2/';
  next_url: string = '';
  previous_url: string = ''; 

  constructor(
    private pokemonService: PokemonAPIService,
    private router: Router  
    ) { }

  ngOnInit(): void {
    this.getInitialPokemons()
  }

  //gets the next or previous 50 pokemons
  async loadNextList(direction:string): Promise<void> {
    let url : string = '';

    //Check which list to get and set the url to call
    if(direction === 'Next'){
      if (this.next_url === ''){
        alert('End of list');
        return;
      }
      url = this.next_url;
    }
    else {
      if (this.previous_url === ''){
        alert('First page. There is no previous');
        return;
      }
      url = this.previous_url;
    }

    //empty the list
    this.pokemonList = [];

    var pokemons = await this.pokemonService.getNextPokemons(url);
    this.setParameters(pokemons);
  }


  //gets the first 50 pokemons
  async getInitialPokemons(): Promise<void> {
    var pokemons: PokemonInfo = await this.pokemonService.getPokemons();
    this.setParameters(pokemons);
  }

  //Set the list of pokemons displayed and the routing parameters
  setParameters(pokemons): void {
    this.next_url = pokemons.next_url;
    this.previous_url = pokemons.previous_url;
    this.pokemonList = pokemons.pokemonList;
  }

  //searches for pokemons containing a name
  async searchForPokemons(): Promise<void> {
    var inputValue = (<HTMLInputElement>document.getElementById("searchName")).value;
    var pokemons: PokemonInfo;

    //gets the first 50 if one search for an empty list
    if(inputValue === '') 
    {
      pokemons = await this.pokemonService.getPokemons();
    }
    //Gets all the pokemons that contains the search word
    else {
      pokemons = await this.pokemonService.getAllPokemonsWithName(inputValue);
    }
    this.setParameters(pokemons);
  }


  //Set the background color based on the type of pokemon
  setTdBackgroundColor(type) {
    switch (type) {
      case 'grass': 
        return '#35e935';
      case 'fire': 
        return '#f15a5a';
      case 'poison': 
        return '#cb35e9';
      case 'flying': 
        return '#7c47dd';
      case 'bug': 
        return '#57701b';
      case 'normal': 
        return '#63621f';
      case 'dark': 
        return '#36362e';
      case 'electric': 
        return '#e9d735';
      case 'psychic': 
        return '#e935bc';
      case 'ground': 
        return '#8d8d58';
      case 'ice': 
        return '#35dde9';
      case 'steel': 
        return '#919191';
      case 'fairy': 
        return '#d87fd1';
      case 'water': 
        return '#3550e9';
      case 'fighting': 
        return '#e93535';
      case 'rock': 
        return '#4f522a';
      case 'ghost': 
        return '#4e0c7a';
      case 'dragon': 
        return '#404ea1';
      default:
        return '#ffffff'
    }
  }
}
