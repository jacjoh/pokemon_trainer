import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { PokemonAPIService } from 'src/app/services/pokemon-api.service';
import { TrainerPokemonsService } from 'src/app/services/trainer-pokemons.service';
import { PokemonListData } from '../interfaces/pokemonListData';

@Component({
  selector: 'app-trainer-page',
  templateUrl: './trainer-page.component.html',
  styleUrls: ['./trainer-page.component.scss']
})
export class TrainerPageComponent implements OnInit {
  storedPokemonIds: Array<string>;
  pokemonList: Array<PokemonListData>;

  constructor(
    private trainerService: TrainerPokemonsService,
    private pokemonService: PokemonAPIService,
    private router: Router  
  ) { }

  async ngOnInit(): Promise<void> {
    this.storedPokemonIds = this.trainerService.getPokemons();
    if(this.storedPokemonIds.length != 0){
      this.pokemonList = await this.pokemonService.getListOfPokemonsById(this.storedPokemonIds);
    }
  }

  goToPokemonList(): void {
    this.router.navigate(['/pokemon-list']);
  }

  setTdBackgroundColor(type) {
    switch (type) {
      case 'grass': 
        return '#35e935';
      case 'fire': 
        return '#f15a5a';
      case 'poison': 
        return '#cb35e9';
      case 'flying': 
        return '#7c47dd';
      case 'bug': 
        return '#57701b';
      case 'normal': 
        return '#63621f';
      case 'dark': 
        return '#36362e';
      case 'electric': 
        return '#e9d735';
      case 'psychic': 
        return '#e935bc';
      case 'ground': 
        return '#8d8d58';
      case 'ice': 
        return '#35dde9';
      case 'steel': 
        return '#919191';
      case 'fairy': 
        return '#d87fd1';
      case 'water': 
        return '#3550e9';
      case 'fighting': 
        return '#e93535';
      case 'rock': 
        return '#4f522a';
      case 'ghost': 
        return '#4e0c7a';
      case 'dragon': 
        return '#404ea1';
      default:
        return '#ffffff'
    }
  }
}
