import { PokemonListData } from './pokemonListData';

export interface PokemonInfo {
    next_url: string,
    previous_url: string,
    pokemonList: Array<PokemonListData>
}
