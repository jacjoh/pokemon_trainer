import { Sprites } from './sprites';

export interface Pokemon {
    id: number;
    abilities: Array<any>;
    base_experience: number;
    forms: Array<any>;
    game_indices: Array<any>;
    height: number;
    held_items: Array<any>;
    is_default: Boolean;
    location_area_encounters: string;
    moves: Array<any>;
    name: string;
    order: number;
    species: Array<any>;
    sprites: Sprites;
    stats: Array<any>;
    types: Array<any>;
    weight: number;
  }
  