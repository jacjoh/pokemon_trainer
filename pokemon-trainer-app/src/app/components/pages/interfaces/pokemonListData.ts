export interface PokemonListData {
    id: number;
    name: string;
    sprite: string;
    types: Array<any>;
}