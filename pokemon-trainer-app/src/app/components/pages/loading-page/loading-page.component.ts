import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-loading-page',
  templateUrl: './loading-page.component.html',
  styleUrls: ['./loading-page.component.scss']
})

export class LoadingPageComponent implements OnInit {
  trainerName: string = '';
  constructor(
    private router: Router  
    ) { }

  ngOnInit(): void {
    this.trainerName = localStorage.getItem('trainerName');
  }

  //Stores the trainer name in localstorage
  submitName(): void {
    localStorage.setItem('trainerName', this.trainerName);
    this.router.navigate(['/pokemon-list']);
  }

  //Set the name when the user inputs text to the text box
  changedName(event: any): void {
    this.trainerName = event.target.value;
  }

}
